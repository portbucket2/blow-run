using Unity.Entities;

public struct BallData : IComponentData
{
    public float inverseMass;
    public bool launched;
}

public struct BallOnFanData : IComponentData
{

}