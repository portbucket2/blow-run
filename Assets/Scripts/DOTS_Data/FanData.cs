using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct FanData : IComponentData
{
    public float3 inDir;
    public float3 outDir;

    public int id;

    public float pullForce;

    public float pushForce;

    public bool on;
}