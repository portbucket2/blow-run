using Unity.Entities;
using Unity.Physics;

public struct LaunchData : IComponentData
{
    public bool launched;
    public float launchTime;
    public float randomScale;
    public bool colliderRemoved;
    public bool colliderReattached;
    public PhysicsCollider physicsCollider;
}
