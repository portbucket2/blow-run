using Unity.Entities;

public struct DestructibleData : IComponentData
{
    public float lifeTime;
}
