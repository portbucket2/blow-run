using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct PullFanData : IComponentData
{
    public float pullForce;
    public float3 inDir;
}
