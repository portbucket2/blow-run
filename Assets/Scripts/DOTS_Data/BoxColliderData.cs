using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct BoxColliderData : IComponentData
{
    public bool top;
    public bool bottom;
    public float3 startingSize;
    public float3 startingCenter;
}
