using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Physics;
using BoxCollider = Unity.Physics.BoxCollider;
using PathCreation.Examples;

public class Fan : MonoBehaviour
{
    [Header("Spawn Point:")]
    public Transform spawnPoint;

    [Header("Move:")]
    //[SerializeField] float forwardSpeed;
    [SerializeField] float sideSpeed;
    [SerializeField] float maxAllowableSideMovement;

    [Header("Blade:")]
    [SerializeField] Transform blade;
    [SerializeField] float bladeRotationSpeed;

    [Header("Pickup Effect:")]
    [SerializeField] float enlargement;
    [SerializeField] float shrinking;

    public int currentCapacity { get; set; }

    Vector3 prevTouch;
    Vector3 touchDelta;

    Vector3 prevPos;
    public Vector3 currentVelocity { get; private set; }

    [Header("Test Boxes:")]
    [SerializeField] UnityEngine.BoxCollider[] boxes;

    [Header("Path Movement:")]
    [SerializeField] PathFollower pathFollower;

    [Header("RGB:")]
    [SerializeField] float colorChangeSpeed;
    float h;
    [SerializeField] MeshRenderer rgbStrip;
    UnityEngine.Material rgbMat;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        currentCapacity = LevelManager.Instance.startingCapacity;
        gameObject.layer = LayerMask.NameToLayer("Fan");
        rgbMat = rgbStrip.materials[1];

        prevPos = transform.position;
        yield return null;
        InitializeContainer();
    }

    // Update is called once per frame
    void Update()
    {
        //Blade
        blade.transform.Rotate(blade.forward, bladeRotationSpeed * Time.deltaTime, Space.World);

        //RGB:
        rgbMat.SetColor("_BaseColor", Color.HSVToRGB(Mathf.Repeat(h += colorChangeSpeed * Time.deltaTime, 1f), 0.8f, 1f));

        //Input:
        if (Input.GetMouseButtonDown(0))
        {
            prevTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            //Drag Calculation:
            Vector3 currentTouch = Input.mousePosition;
            touchDelta = currentTouch - prevTouch;
            touchDelta.x /= Screen.width;
            touchDelta.y /= Screen.height;
            prevTouch = currentTouch;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            touchDelta = Vector3.zero;
        }

        //Fan Movement:
        float sideMovement = touchDelta.x * sideSpeed;
        //Debug.Log(movementMag);
        sideMovement = Mathf.Clamp(sideMovement, -maxAllowableSideMovement, maxAllowableSideMovement);
        pathFollower.sideOffset += sideMovement * Time.deltaTime;

        //Fan Velocity:
        currentVelocity = (transform.position - prevPos) / Time.deltaTime;
        prevPos = transform.position;
    }

    public void PositivePickupEffect(int value = 1)
    {
        switch (ABManager.Instance.Gameplay_Version)
        {
            case ABManager.AB_Version.OG:
                Debug.Log("Enlarge");
                LeanTween.scale(gameObject, transform.localScale * enlargement, 0.5f).setEase(LeanTweenType.easeOutBack);
                EnlargeContainer(enlargement);
                //EnlargeContainerMono(enlargement);
                ChangeCapacity(true);
                break;

            case ABManager.AB_Version.NoGate:
                AddOrRemoveBalls(value);
                break;

            default:
                break;
        }
    }

    public void NegativePickupEffect(int value = -1)
    {
        switch (ABManager.Instance.Gameplay_Version)
        {
            case ABManager.AB_Version.OG:
                Debug.Log("Shrink");
                LeanTween.scale(gameObject, transform.localScale / enlargement, 0.5f).setEase(LeanTweenType.easeInBack);
                ShrinkContainer(shrinking);
                //ShrinkContainerMono(shrinking);
                ChangeCapacity(false);
                break;

            case ABManager.AB_Version.NoGate:
                AddOrRemoveBalls(value);
                break;

            default:
                break;
        }

    }

    public void PerformGateOperation(Operation operation)
    {
        int ballDelta = operation.DeltaBalls(GetNumberOfBallsOnFan());

        if (ballDelta > 0)
        {
            LevelManager.Instance.SpawnBalls(ballDelta);
        }
        else if (ballDelta < 0)
        {
            LevelManager.Instance.DestroyBalls(Mathf.Abs(ballDelta));
        }
    }

    public void AddOrRemoveBalls(int ballDelta)
    {
        if (ballDelta > 0)
        {
            LevelManager.Instance.SpawnBalls(ballDelta);
        }
        else
        {
            LevelManager.Instance.DestroyBalls(ballDelta);
        }
    }

    public int GetNumberOfBallsOnFan()
    {
        NativeArray<Entity> balls = GetBallsOnFan();
        int number = balls.Length;
        balls.Dispose();
        return number;
    }

    public NativeArray<Entity> GetBallsOnFan(Unity.Collections.Allocator allocation = Unity.Collections.Allocator.Temp)
    {
        return LevelManager.entityManager.CreateEntityQuery(ComponentType.ReadOnly<BallOnFanData>()).ToEntityArray(allocation);
    }

    void InitializeContainer()
    {
        NativeArray<Entity> boxes = LevelManager.entityManager.CreateEntityQuery(ComponentType.ReadOnly<BoxColliderData>()).ToEntityArray(Allocator.Temp);

        for (int i = 0; i < boxes.Length; i++)
        {
            BoxColliderData colData = LevelManager.entityManager.GetComponentData<BoxColliderData>(boxes[i]);
            PhysicsCollider col = LevelManager.entityManager.GetComponentData<PhysicsCollider>(boxes[i]);
            unsafe
            {
                var scPtr = (BoxCollider*)col.ColliderPtr;
                var geometry = scPtr->Geometry;
                geometry.Center = colData.startingCenter;
                geometry.Size = colData.startingSize;
                scPtr->Geometry = geometry;
            }
        }

        boxes.Dispose();
    }

    public void DeactivateContainer()
    {
        NativeArray<Entity> boxes = LevelManager.entityManager.CreateEntityQuery(ComponentType.ReadOnly<BoxColliderData>()).ToEntityArray(Allocator.Temp);

        for (int i = 0; i < boxes.Length; i++)
        {
            LevelManager.entityManager.RemoveComponent<PhysicsCollider>(boxes[i]);
        }

        boxes.Dispose();
    }

    void EnlargeContainer(float byAmount)
    {
        NativeArray<Entity> boxes = LevelManager.entityManager.CreateEntityQuery(ComponentType.ReadOnly<BoxColliderData>()).ToEntityArray(Allocator.Temp);

        if (shrunkTimes == 0)   //Actual expanding
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                BoxColliderData colData = LevelManager.entityManager.GetComponentData<BoxColliderData>(boxes[i]);
                PhysicsCollider col = LevelManager.entityManager.GetComponentData<PhysicsCollider>(boxes[i]);
                unsafe
                {
                    var scPtr = (BoxCollider*)col.ColliderPtr;
                    var geometry = scPtr->Geometry;
                    float3 newSize = new float3(geometry.Size.x, geometry.Size.y + 0.5f * enlargement, geometry.Size.z + 0.5f * enlargement);
                    //Debug.Log(" Entity " + boxes[i] + " Size: " + geometry.Size + " -> " + newSize);
                    geometry.Size = newSize;
                    if (!(colData.top || colData.bottom))
                    {
                        geometry.Center += new float3(0.25f * enlargement, 0.25f * enlargement, 0f);
                    }
                    else if (colData.top)
                    {
                        geometry.Center += new float3(0.5f * enlargement, 0f, 0f);
                    }
                    scPtr->Geometry = geometry;
                }
            }
        }
        else    //Reverse shrinking
        {
            shrunkTimes--;
            for (int i = 0; i < boxes.Length; i++)
            {
                BoxColliderData colData = LevelManager.entityManager.GetComponentData<BoxColliderData>(boxes[i]);
                PhysicsCollider col = LevelManager.entityManager.GetComponentData<PhysicsCollider>(boxes[i]);
                unsafe
                {
                    var scPtr = (BoxCollider*)col.ColliderPtr;
                    var geometry = scPtr->Geometry;
                    //Debug.Log(" Entity " + boxes[i] + " Size: " + geometry.Size + " -> " + newSize);
                    if (!colData.bottom)
                    {
                        geometry.Size = new float3(geometry.Size.x - 0.5f * shrinking, geometry.Size.y - 0.5f * shrinking, geometry.Size.z);
                    }
                    else
                    {
                        geometry.Size = new float3(geometry.Size.x * shrinking, geometry.Size.y, geometry.Size.z);
                    }
                    scPtr->Geometry = geometry;
                }
            }
        }

        boxes.Dispose();
    }

    int shrunkTimes = 0;
    void ShrinkContainer(float byAmount)
    {
        shrunkTimes++;
        NativeArray<Entity> boxes = LevelManager.entityManager.CreateEntityQuery(ComponentType.ReadOnly<BoxColliderData>()).ToEntityArray(Allocator.Temp);

        for (int i = 0; i < boxes.Length; i++)
        {
            BoxColliderData colData = LevelManager.entityManager.GetComponentData<BoxColliderData>(boxes[i]);
            PhysicsCollider col = LevelManager.entityManager.GetComponentData<PhysicsCollider>(boxes[i]);
            unsafe
            {
                var scPtr = (BoxCollider*)col.ColliderPtr;
                var geometry = scPtr->Geometry;
                //Debug.Log(" Entity " + boxes[i] + " Size: " + geometry.Size + " -> " + newSize);
                if (!colData.bottom)
                {
                    geometry.Size = new float3(geometry.Size.x + 0.5f * shrinking, geometry.Size.y + 0.5f * shrinking, geometry.Size.z);
                }
                else
                {
                    geometry.Size = new float3(geometry.Size.x / shrinking, geometry.Size.y, geometry.Size.z);
                }
                scPtr->Geometry = geometry;
            }
        }

        boxes.Dispose();
    }

    void EnlargeContainerMono(float byAmount)
    {
        for (int i = 0; i < boxes.Length; i++)
        {
            UnityEngine.BoxCollider box = boxes[i];
            Vector3 newSize = new float3(box.size.x, box.size.y + 0.5f * enlargement, box.size.z + 0.5f * enlargement);
            box.size = newSize;
            if (box.CompareTag("Player"))   //side walls
            {
                box.center += new Vector3(0.25f * enlargement, 0.25f * enlargement, 0f);
            }
            if (box.CompareTag("Respawn"))  //top wall
            {
                box.center += new Vector3(0.5f * enlargement, 0f, 0f);
            }
        }
    }

    void ShrinkContainerMono(float byAmount)
    {
        for (int i = 0; i < boxes.Length; i++)
        {
            UnityEngine.BoxCollider box = boxes[i];
            Vector3 newSize = new float3(box.size.x, box.size.y - 0.5f * enlargement, box.size.z - 0.5f * enlargement);
            box.size = newSize;
            if (box.CompareTag("Player"))   //side walls
            {
                box.center -= new Vector3(0.25f * enlargement, 0.25f * enlargement, 0f);
            }
            if (box.CompareTag("Respawn"))  //top wall
            {
                box.center -= new Vector3(0.5f * enlargement, 0f, 0f);
            }
        }
    }

    void ChangeCapacity(bool increase)
    {
        if (ABManager.Instance.Gameplay_Version != ABManager.AB_Version.OG) return;

        if (increase)
        {
            currentCapacity += (int)(currentCapacity * LevelManager.Instance.capacityChangeFactor);
        }
        else
        {
            currentCapacity -= (int)(currentCapacity * LevelManager.Instance.capacityChangeFactor);
        }

        LevelManager.Instance.CapacityEvaluation();
    }
}
