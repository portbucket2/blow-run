using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatePickup : PickupWrapper.Pickup
{
    [SerializeField] TMPro.TMP_Text operation_Text;
    public Operation gateOperation;
    [SerializeField] ParticleSystem gatePS;
    [SerializeField] GatePickup otherGateOfPair;
    [SerializeField] MeshRenderer mesh;
    [SerializeField] BoxCollider col;

    // Start is called before the first frame update
    void Start()
    {
        SetGateOperation(gateOperation);
    }

    public override void PickupBehaviour()
    {
        Debug.Log("Gate");
        LevelManager.Instance.fan.PerformGateOperation(gateOperation);
        otherGateOfPair.DisableGate();
    }

    public void SetGateOperation(Operation operation)
    {
        operation_Text.text = operation.ToString();
        gateOperation = operation;
    }

    public override void VisualizePickup()
    {
    }

    public void DisableGate()
    {
        mesh.material.SetColor("_COLOR_B", Color.gray);
        col.enabled = false;
    }

    public override void PlayPickupVFX()
    {
        gatePS.Play();
        mesh.gameObject.SetActive(false);
    }
}