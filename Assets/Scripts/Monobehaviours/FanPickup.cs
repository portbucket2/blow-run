using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanPickup : PickupWrapper.Pickup
{
    [SerializeField] int ballDelta;
    public override void PickupBehaviour()
    {
        LevelManager.Instance.fan.PositivePickupEffect(ballDelta);
        LeanTween.cancel(gameObject);
        Destroy(gameObject);
    }

    public override void PlayPickupVFX()
    {

    }

    public override void VisualizePickup()
    {
        LeanTween.scale(gameObject, Vector3.one * 1.2f, 0.7f).setLoopPingPong();
    }
}
