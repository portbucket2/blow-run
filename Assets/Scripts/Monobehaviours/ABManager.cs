using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PotatoSDK;
using UnityEditor;
using System.IO;

public class ABManager : MonoBehaviour
{
    public static ABManager Instance { get; private set; }

    public enum AB_Version
    {
        OG, OnlyGate, NoGate
    }


    int AB_Value
    {
        get
        {
            if (!Potato.IsReady)
            {
                Debug.LogError("ABMan not ready yet!");
                return 0;
            }
            //return ABMan.GetValue_Int(ABtype.AB0_game_mechanic);
            return 1;
        }
    }

    [SerializeField] AB_Version ab_Version;

    [Header("Test:")]
    [SerializeField] bool useCustomABVersion;

    public AB_Version Gameplay_Version
    {
        get
        {
            if (!useCustomABVersion)
            {
                ab_Version = (AB_Version)AB_Value;
            }
            Debug.Log($"Gameplay Version In Use: <color=cyan>{ab_Version}</color>");
            return ab_Version;
        }
    }

    private void Awake()
    {
        Instance = this;
    }
}
