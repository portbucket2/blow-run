using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// public enum ColorID
// {
//     A, B, C, D
// }

[CreateAssetMenu(fileName = "New Color Palette", menuName = "ScriptableObjects/Color Palette")]
public class ColorPalette : ScriptableObject
{
    public Color[] colors;
}


// #if UNITY_EDITOR
// [CustomEditor(typeof(ColorPalette))]
// public class ColorPaletteEditor : Editor
// {
//     ColorPalette colorPalette;

//     SerializedProperty colorID;
//     SerializedProperty color;

//     private void OnEnable()
//     {
//         colorPalette = (ColorPalette)target;
//         colorID = serializedObject.FindProperty("colorIDs");
//         color = serializedObject.FindProperty("colors");
//     }

//     public override void OnInspectorGUI()
//     {
//         serializedObject.Update();

//         GUILayout.BeginHorizontal();

//         EditorGUILayout.PropertyField(colorID);
//         EditorGUILayout.PropertyField(color);

//         GUILayout.EndHorizontal();

//         serializedObject.ApplyModifiedProperties();
//     }
// }
// #endif