using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupWrapper
{
    public abstract class Pickup : MonoBehaviour
    {
        bool triggerEntered;

        private void Awake()
        {
            gameObject.layer = LayerMask.NameToLayer("Pickup");
            VisualizePickup();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!triggerEntered)
            {
                triggerEntered = true;
                PickupBehaviour();
                PlayPickupVFX();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (triggerEntered)
            {
                triggerEntered = false;
            }
        }

        public abstract void PickupBehaviour();

        public abstract void VisualizePickup();
        public abstract void PlayPickupVFX();
    }
}
