using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Physics;
using Unity.Collections;
using Material = UnityEngine.Material;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;
using Unity.Jobs;
using Unity.Burst;
using Cinemachine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }
    public static EntityManager entityManager;

    [Header("References:")]
    public UIManager uiManager;

    [Header("Game Settings:")]
    public float minVelocityThreshold;
    public float velocityMultFactor;

    public bool gamePlayStarted { get; private set; }

    public bool levelEnd { get; private set; }

    public Fan fan;

    [SerializeField] MyUtils.MinMaxFloat[] spawnRange;
    [SerializeField] int unitsToSpawn;
    [SerializeField] GameObject ballPrefab;
    [SerializeField] UnityEngine.Material objectMaterial;
    [SerializeField] MyUtils.MinMaxFloat objectScaleRange;
    [SerializeField] ColorPalette colorPalette;

    [Header("Force:")]
    public MyUtils.MinMaxFloat distanceRange;
    public MyUtils.MinMaxFloat forceRange;
    public float decelaration;

    [Header("Capacity:")]
    public int startingCapacity;
    public float capacityChangeFactor;

    [Header("Pit:")]
    public Transform pit;
    public float launchAngle;
    public float launchSpeed;
    public float ballLaunchInterval;
    [SerializeField] CinemachineVirtualCamera pitCam;
    [SerializeField] float pitCamTransitionDuration;
    [SerializeField] ParticleSystem ballSpawnPS;
    [SerializeField] float ballSpawnPS_StartDelay;
    [SerializeField] float ballClusterPauseTimeFactor;

    public PhysicsCollider colliderState { get; private set; }

    private void Awake()
    {
        Instance = this;
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        // if (GameManager.Instance == null)
        // {
        //     enabled = false;
        //     SceneManager.LoadScene(0);
        // }
    }

    // Start is called before the first frame update
    void Start()
    {
        SystemBase debugStream = World.DefaultGameObjectInjectionWorld.GetExistingSystem<DebugStream>();
        SimulationSystemGroup simulationSystemGroup = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<SimulationSystemGroup>();
        if (debugStream != null)
        {
            debugStream.Enabled = false;
            simulationSystemGroup.RemoveSystemFromUpdateList(debugStream);
            //World.DefaultGameObjectInjectionWorld.DestroySystem(debugStream);
        }

        SpawnBalls(unitsToSpawn);

        gamePlayStarted = true;
        //Debug.Break();
    }

    public void SpawnBalls(int number)
    {
        using (BlobAssetStore blob = new BlobAssetStore())
        {
            GameObjectConversionSettings conversionSettings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blob);
            Entity entityToSpawn = GameObjectConversionUtility.ConvertGameObjectHierarchy(ballPrefab, conversionSettings);

            int colorCount = colorPalette.colors.Length;
            //int unitsToSpawnOfEachColor = (int)((float)number / (float)colorCount);

            Material[] mats = new Material[colorCount];
            for (int i = 0; i < colorCount; i++)
            {
                Material mat = new UnityEngine.Material(objectMaterial);
                mat.SetColor("_Color", colorPalette.colors[i]);
                mats[i] = mat;
            }

            for (int k = 0; k < number; k++)
            {
                Entity spawnedEntity = entityManager.Instantiate(entityToSpawn);
                float3 spawnOrigin = fan.spawnPoint.position;
                float3 spawnPoint = spawnOrigin + new float3(spawnRange[0].GetRandomInRange(), spawnRange[1].GetRandomInRange(), spawnRange[2].GetRandomInRange());
                float3 randRotation = new float3(Random.Range(-180f, 180f), Random.Range(-180f, 180f), Random.Range(-180f, 180f));

                entityManager.SetComponentData(spawnedEntity, new Translation { Value = spawnPoint });
                entityManager.SetComponentData(spawnedEntity, new Rotation { Value = quaternion.Euler(randRotation) });
                //entityManager.AddComponentData(spawnedEntity, new MaterialPropertyColor { Value = new float4(color.r, color.g, color.b, color.a) });
                RenderMesh renderMesh = entityManager.GetSharedComponentData<RenderMesh>(spawnedEntity);
                renderMesh.material = mats[Random.Range(0, mats.Length)];
                entityManager.SetSharedComponentData<RenderMesh>(spawnedEntity, renderMesh);

                float inverseMass = entityManager.GetComponentData<PhysicsMass>(spawnedEntity).InverseMass;
                entityManager.AddComponentData(spawnedEntity, new BallData
                {
                    inverseMass = inverseMass,
                    launched = false
                });
                entityManager.AddComponentData(spawnedEntity, new BallOnFanData { });

                float3 randomScale = Vector3.one * objectScaleRange.GetRandomInRange();
                entityManager.AddComponentData(spawnedEntity, new NonUniformScale { Value = randomScale });
            }
        }


        //CapacityEvaluation();
    }

    public void DestroyBalls(int number)
    {
        NativeArray<Entity> ballsOnFan = fan.GetBallsOnFan();
        for (int i = 0; i < number; i++)
        {
            entityManager.DestroyEntity(ballsOnFan[i]);
        }
        ballsOnFan.Dispose();

        CapacityEvaluation();
    }

    public void DeactivateBallColliders(int number)
    {
        NativeArray<Entity> ballsOnFan = fan.GetBallsOnFan();
        for (int i = 0; i < number; i++)
        {
            entityManager.RemoveComponent<BallOnFanData>(ballsOnFan[i]);
            entityManager.RemoveComponent<PhysicsCollider>(ballsOnFan[i]);
            entityManager.AddComponentData<DestructibleData>(ballsOnFan[i], new DestructibleData { lifeTime = 3f });
        }
        ballsOnFan.Dispose();
        // PhysicsCollider pc = colliders[collisionEvent.EntityB];
        // unsafe
        // {
        //     Unity.Physics.Collider* c1 = pc.ColliderPtr;
        //     Unity.Physics.CollisionFilter cf1 = c1->Filter;
        //     //cf1.BelongsTo = 1u << (2);
        //     cf1.CollidesWith = 1 << 3;
        //     c1->Filter = cf1;
        // }
    }

    public void CapacityEvaluation()
    {
        if (ABManager.Instance.Gameplay_Version != ABManager.AB_Version.OG) return;

        int currentBalls = fan.GetNumberOfBallsOnFan();
        if (currentBalls > fan.currentCapacity)
        {
            int capacityDelta = currentBalls - fan.currentCapacity;
            DeactivateBallColliders(capacityDelta);
            currentBalls -= capacityDelta;
        }
        uiManager.UpdateUI(fan.currentCapacity, currentBalls);
    }

    public void StartFillingBallPit()
    {
        pitCam.Priority = 1000;
        LeanTween.delayedCall(pitCamTransitionDuration, () =>
        {
            BallPitSequence();
        });
    }

    private void BallPitSequence()
    {
        SimulationSystemGroup simulationSystemGroup = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<SimulationSystemGroup>();

        LaunchSystem launchSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<LaunchSystem>();
        if (launchSystem != null)
        {
            launchSystem.Enabled = true;
            simulationSystemGroup.AddSystemToUpdateList(launchSystem);
        }

        ColliderModificaitonSystem colliderModificaitonSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<ColliderModificaitonSystem>();
        if (colliderModificaitonSystem != null)
        {
            colliderModificaitonSystem.Enabled = true;
            simulationSystemGroup.AddSystemToUpdateList(colliderModificaitonSystem);
        }

        NativeArray<Entity> balls = fan.GetBallsOnFan(Allocator.Temp);
        int numberOfBalls = balls.Length;
        EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
        for (int i = 0; i < numberOfBalls; i++)
        {
            LaunchInitJob job = new LaunchInitJob
            {
                ball = balls[i],
                ecb = ecb,
                launchInterval = ballLaunchInterval,
                index = i
            };
            job.Run();
        }
        ecb.Playback(entityManager);
        ecb.Dispose();
        balls.Dispose();

        LeanTween.delayedCall(ballSpawnPS_StartDelay, () =>
        {
            ballSpawnPS.Play();
            LeanTween.delayedCall(ballClusterPauseTimeFactor * numberOfBalls, () =>
            {
                ballSpawnPS.Pause();
                LeanTween.delayedCall(1f, () => LevelComplete());
            });
        });
    }


    public void LevelComplete()
    {
        uiManager.ShowLevelCompleteUI();
    }

    public void LevelFailed()
    {

    }

    public void LevelRestart()
    {
        entityManager.DestroyEntity(entityManager.UniversalQuery);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

[BurstCompile]
public struct LaunchJob : IJob
{
    public EntityManager manager;
    public EntityCommandBuffer ecb;

    public Entity ball;
    [ReadOnly]
    public Translation position;
    public float3 pitPos;
    public float launchAngle;
    public float launchSpeed;
    public float randomScale;

    public void Execute()
    {
        float3 dir = math.normalize(pitPos - position.Value);
        dir.y = 0f;
        float launchAngleRad = UnityEngine.Mathf.Deg2Rad * launchAngle;
        float3 launchDir = math.cos(launchAngleRad) * dir + math.sin(launchAngleRad) * math.up();
        PhysicsGravityFactor gravity = new PhysicsGravityFactor { Value = 1f };
        PhysicsVelocity physicsVel = new PhysicsVelocity { Linear = launchSpeed * launchDir };

        ecb.RemoveComponent<PhysicsCollider>(ball);
        manager.SetComponentData<PhysicsGravityFactor>(ball, gravity);
        manager.SetComponentData<PhysicsVelocity>(ball, physicsVel);
        manager.SetComponentData<NonUniformScale>(ball, new NonUniformScale { Value = randomScale });
    }
}

[BurstCompile]
public struct LaunchInitJob : IJob
{
    [ReadOnly]
    public Entity ball;
    [WriteOnly]
    public EntityCommandBuffer ecb;
    [ReadOnly]
    public float launchInterval;
    [ReadOnly]
    public float index;
    public void Execute()
    {
        ecb.AddComponent<LaunchData>(ball, new LaunchData
        {
            launched = false,
            launchTime = index * launchInterval,
            randomScale = Unity.Mathematics.Random.CreateFromIndex((uint)index).NextFloat(3f, 5f)
        });
    }
}
