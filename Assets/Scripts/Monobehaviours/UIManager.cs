using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Test:")]
    [SerializeField] TMP_Text ballCapacity;
    [SerializeField] TMP_Text balls;

    [Header("Level Canvas:")]
    [SerializeField] GameObject levelCompletePanel;

    [Header("Buttons:")]
    [SerializeField] Button reset_Button;

    // Start is called before the first frame update
    void Start()
    {
        ConfigureUIButtons();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ConfigureUIButtons()
    {
        reset_Button.onClick.AddListener(() => LevelManager.Instance.LevelRestart());
    }

    public void UpdateUI(int capacity, int ballCount)
    {
        ballCapacity.text = capacity.ToString();
        balls.text = ballCount.ToString();
    }

    public void ShowLevelCompleteUI()
    {
        levelCompletePanel.SetActive(true);
    }
}
