using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS_Recorder : MonoBehaviour
{
    [Range(0f, 1f)]
    public float interval = 0.1f;
    public bool colorCode = true;
    string color;
    const string RED = "red";
    const string GREEN = "green";
    const string YELLOW = "yellow";

    [SerializeField] TMPro.TMP_Text fps_Text;
    WaitForSeconds waitForInterval;

    private void Awake()
    {
        waitForInterval = new WaitForSeconds(interval);
        if (interval == 0f)
        {
            waitForInterval = null;
        }

        if (GameManager.Instance != null && GameManager.Instance.showFPS)
        {
            enabled = true;
            fps_Text.gameObject.SetActive(true);
        }
        else
        {
            enabled = false;
            fps_Text.gameObject.SetActive(false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Show_FPS());
    }

    IEnumerator Show_FPS()
    {
        while (true)
        {
            int fps = (int)(1f / Time.deltaTime);
            if (colorCode)
            {
                if (fps >= 50)
                {
                    color = GREEN;
                }
                else if (fps >= 30)
                {
                    color = YELLOW;
                }
                else
                {
                    color = RED;
                }
                fps_Text.text = $"<color={color}>{fps}</color>";
            }
            else
            {
                fps_Text.text = $"{fps}";
            }
            yield return waitForInterval;
        }
    }
}
