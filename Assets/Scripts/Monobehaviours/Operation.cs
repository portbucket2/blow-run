using UnityEngine;

[System.Serializable]
public class Operation
{
    public Operation(OperationType operationType, float number)
    {
        this.operationType = operationType;
        this.number = number;
    }

    public enum OperationType
    {
        Addition, Subtraction, Multiplication, Division
    }

    public OperationType operationType;
    public float number;

    public override string ToString()
    {
        int symbolSpriteIndex = 0;
        switch (this.operationType)
        {
            case OperationType.Addition:
                symbolSpriteIndex = 0;
                break;
            case OperationType.Subtraction:
                symbolSpriteIndex = 1;
                break;
            case OperationType.Multiplication:
                symbolSpriteIndex = 2;
                break;
            case OperationType.Division:
                symbolSpriteIndex = 3;
                break;
            default:
                break;
        }

        return $"<sprite={symbolSpriteIndex}>{this.number}";
    }

    public float Result(int existingBalls)
    {
        float result = 0f;
        switch (this.operationType)
        {
            case OperationType.Addition:
                result = existingBalls + this.number;
                break;
            case OperationType.Subtraction:
                result = Mathf.Clamp(existingBalls - this.number, 0f, 10000f);
                break;
            case OperationType.Multiplication:
                result = existingBalls * this.number;
                break;
            case OperationType.Division:
                result = existingBalls / this.number;
                break;
            default:
                break;
        }

        return result;
    }

    public int DeltaBalls(int existingBalls)
    {
        int resultingBalls = (int)Result(existingBalls);
        return resultingBalls - existingBalls;
    }
}

