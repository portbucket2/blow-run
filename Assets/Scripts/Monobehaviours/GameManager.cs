using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }


    [Header("Game Settings:")]
    [SerializeField] int targetFPS = -1;

    public int CurrentLevel { get { return currentLevel; } }
    [SerializeField] int numberOfLevels;
    public int NumberOfLevels { get { return numberOfLevels; } }
    [SerializeField] int nonLevelScenes;

    private readonly string LEVEL = "level";
    private readonly string COIN_PrefKey = "coin";

    [Header("Currency:")]
    public int startingCoin;
    public int coinAmount { get; set; }

    [Header("Test:")]
    public bool showFPS;
    [SerializeField] bool customLevel;
    [SerializeField] int currentLevel;
    [SerializeField] bool lockLevel;

    public int SceneToLoad
    {
        get
        {
            return ((currentLevel - 1) % numberOfLevels) + nonLevelScenes;
        }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }

        coinAmount = PlayerPrefs.GetInt(COIN_PrefKey, startingCoin);
        if (!customLevel)
        {
            currentLevel = GetCurrentLevel();
        }

        Application.targetFrameRate = targetFPS;
    }

    IEnumerator Start()
    {
        while (!PotatoSDK.Potato.IsReady)
        {
            yield return null;
        }

        Debug.Log("Game Started");

        //MyEventManager.LogGameStart();
        //MyEventManager.HideDebugger();

        SceneManager.LoadScene(SceneToLoad);
    }

    public void IncreaseLevel()
    {
        if (customLevel && lockLevel)
            return;

        currentLevel++;
        PlayerPrefs.SetInt(LEVEL, currentLevel);
    }

    public void DecreaseLevel()
    {
        if (customLevel && lockLevel)
            return;

        currentLevel--;
        PlayerPrefs.SetInt(LEVEL, currentLevel);
    }

    private int GetCurrentLevel()
    {
        if (PlayerPrefs.HasKey(LEVEL))
        {
            return PlayerPrefs.GetInt(LEVEL);
        }
        else
        {
            return 1;
        }
    }

    public void AddCoin(int addition)
    {
        //LevelManager.Instance.uiManager.ShowCoinAddition(coinAmount, addition);
        coinAmount += addition;
        PlayerPrefs.SetInt(COIN_PrefKey, coinAmount);
    }
}
