// using UnityEngine;
// using GameAnalyticsSDK;
// using LionStudios.Suite.Analytics;
// using LionStudios.Suite.Debugging;

// public class MyEventManager
// {
//     public static void LogGameStart()
//     {
//         LionAnalytics.GameStart();
//     }

//     public static void HideDebugger()
//     {
//         LionDebugger.Hide();
//     }

//     public static void LogGACustomDimension()
//     {
//         GameAnalytics.SetCustomDimension01(ABManager.AB_Value == 0 ? "old" : "new");
//     }

//     public static void LogLevelStarted(int level)
//     {
//         //Editor
//         Debug.Log(string.Format("<b>Level Started:    </b><color=yellow>{0}</color>", level.ToString("00")));

//         //LionAnalytics:
//         LionAnalytics.LevelStart(level, 0);
//     }

//     public static void LogLevelComplete(int level)
//     {
//         //Editor
//         Debug.Log(string.Format("<b>Level Completed:    </b><color=green>{0}</color>", level.ToString("00")));

//         //LionAnalytics:
//         LionAnalytics.LevelComplete(level, 0);
//     }

//     public static void LogLevelFailed(int level)
//     {
//         //Editor
//         Debug.Log(string.Format("<b>Level Failed:    </b><color=red>{0}</color>", level.ToString("00")));

//         //LionAnalytics:
//         LionAnalytics.LevelFail(level, 0);
//     }

//     public static void LogLevelRestart(int level)
//     {
//         //Editor
//         Debug.Log(string.Format("<b>Level Restarted:    </b><color=#ee82ee>{0}</color>", level.ToString("00")));

//         //LionAnalytics:
//         LionAnalytics.LevelRestart(level, 0);
//     }
// }
