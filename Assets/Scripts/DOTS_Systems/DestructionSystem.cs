using Unity.Entities;
using Unity.Jobs;

[AlwaysSynchronizeSystem]
public class DestructionSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = UnityEngine.Time.deltaTime;
        EntityCommandBuffer ecb = new EntityCommandBuffer(Unity.Collections.Allocator.TempJob);

        Entities.ForEach((Entity entity, ref DestructibleData destructibleData) =>
        {
            destructibleData.lifeTime -= deltaTime;
            if (destructibleData.lifeTime <= 0f)
            {
                ecb.DestroyEntity(entity);
            }
        }).Run();

        ecb.Playback(EntityManager);
        ecb.Dispose();

        return default;
    }
}
