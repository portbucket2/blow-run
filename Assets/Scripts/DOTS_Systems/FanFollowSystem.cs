using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using UnityEngine;

[AlwaysSynchronizeSystem]
public class FanFollowSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        if (LevelManager.Instance == null)
            return default;
        else if (!LevelManager.Instance.gamePlayStarted)
            return default;

        LevelManager levelManager = LevelManager.Instance;
        Transform fan = levelManager.fan.transform;

        Entities.WithoutBurst().ForEach((ref FanData fanData, ref Translation translation, ref Rotation rotation) =>
        {
            translation.Value = fan.position;
            rotation.Value = fan.rotation;
        }).Run();


        return default;
    }
}
