using Unity.Entities;
using Unity.Physics;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Physics.Systems;
using Unity.Collections;
using Unity.Burst;

[AlwaysSynchronizeSystem]
[UpdateBefore(typeof(EndFramePhysicsSystem))]
public class ForceSystem : JobComponentSystem
{
    //[ReadOnly] EntityManager entityManager;
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        if (LevelManager.Instance == null)
            return default;
        else if (!LevelManager.Instance.gamePlayStarted)
            return default;

        EntityManager entityManager = LevelManager.entityManager;

        float velThreshold = LevelManager.Instance.minVelocityThreshold;
        float velMultFact = LevelManager.Instance.velocityMultFactor;
        float decelaration = LevelManager.Instance.decelaration;
        float deltaTime = UnityEngine.Time.deltaTime;

        EntityQuery fanQuery = GetEntityQuery(ComponentType.ReadOnly<FanData>());
        NativeArray<Entity> fans = fanQuery.ToEntityArray(Allocator.TempJob);

        float minForce = LevelManager.Instance.forceRange.min;
        float maxForce = LevelManager.Instance.forceRange.max;
        float minDist = LevelManager.Instance.distanceRange.min;
        float maxDist = LevelManager.Instance.distanceRange.max;

        float3 fanVel = LevelManager.Instance.fan.currentVelocity;

        JobHandle job = Entities.ForEach((Entity entity, ref PhysicsVelocity physicsVel, ref Translation position, in BallData ball, in Rotation rotation) =>
            {
                float velMag = math.length(physicsVel.Linear);
                float3 velDir = math.normalize(physicsVel.Linear);

                if (!ball.launched && velMag < velThreshold)
                {
                    //if(velMag > 0f) 
                    {
                        physicsVel.Linear = math.forward(rotation.Value) * maxForce;
                    }
                    // else
                    // {
                    //     physicsVel.Linear = - velDir * maxForce;
                    // }
                    //physicsVel.Linear = Random.CreateFromIndex((uint)entity.Index).NextFloat3Direction() * maxForce;
                }
                // else 
                // {
                //     physicsVel.Linear -= velDir * decelaration * deltaTime;
                // }
                position.Value += fanVel * deltaTime;
            }).Schedule(inputDeps);

        fans.Dispose();
        job.Complete();
        return job;
    }
}
