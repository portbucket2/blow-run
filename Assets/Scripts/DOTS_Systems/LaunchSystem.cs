using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using UnityEngine;
using Collider = Unity.Physics.Collider;

[DisableAutoCreation]
[UpdateAfter(typeof(ForceSystem))]
//[AlwaysSynchronizeSystem]
public class LaunchSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = UnityEngine.Time.deltaTime;
        float3 pitPos = LevelManager.Instance.pit.position;
        float launchSpeed = LevelManager.Instance.launchSpeed;
        float launchAngle = LevelManager.Instance.launchAngle;
        float launchAngleRad = math.radians(launchAngle);
        float g = 9.81f;
        float cos = math.cos(launchAngleRad);
        //Debug.Log($"cos : {cos}");
        float tan = math.tan(launchAngleRad);


        JobHandle job = Entities.WithBurst().ForEach((Entity entity, ref PhysicsVelocity physicsVel, ref NonUniformScale nonUniformScale,
        ref PhysicsGravityFactor gravity, ref PhysicsCollider physicsCollider, ref LaunchData launchData, ref BallData ballData, in Translation position) =>
        {
            if (!launchData.launched)
            {
                launchData.launchTime -= deltaTime;
                if (launchData.launchTime <= 0f)
                {
                    launchData.launched = true;
                    ballData.launched = true;
                    gravity.Value = 1f;
                    nonUniformScale.Value = launchData.randomScale;
                    float3 flatConnection = new float3(pitPos.x, 0f, pitPos.z) - new float3(position.Value.x, 0f, position.Value.z) +
                    Unity.Mathematics.Random.CreateFromIndex((uint)entity.Index).NextFloat3(0f, 1f);
                    float3 dir = math.normalize(flatConnection);
                    float3 launchDir = math.cos(launchAngleRad) * dir + math.sin(launchAngleRad) * math.up();

                    float x = math.length(flatConnection);
                    //Debug.Log($"x : {x}");
                    float delY = position.Value.y - pitPos.y;
                    //Debug.Log($"dely : {delY}");
                    float launchSpeed = math.sqrt((x * x) / (2f * g * cos * cos * (x * tan + delY))) * 10f;
                    //Debug.Log($"speed : {launchSpeed}");
                    physicsVel.Linear = launchSpeed * launchDir;
                }
            }
        }).Schedule(inputDeps);
        job.Complete();
        return job;
    }
}
