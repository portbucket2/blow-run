using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using UnityEngine;
using Collider = Unity.Physics.Collider;

[DisableAutoCreation]
[AlwaysSynchronizeSystem]
public class ColliderModificaitonSystem : JobComponentSystem
{
    PhysicsCollider physicsColliderState;
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = UnityEngine.Time.deltaTime;

        EntityCommandBuffer ecb = new EntityCommandBuffer(Unity.Collections.Allocator.TempJob);
        EntityManager entityManager = LevelManager.entityManager;

        Entities.WithBurst().ForEach((Entity entity, ref LaunchData launchData, ref BallData ballData, in Translation position) =>
        {
            if (launchData.launched && !launchData.colliderRemoved)
            {
                launchData.physicsCollider = entityManager.GetComponentData<PhysicsCollider>(entity);
                ecb.RemoveComponent<PhysicsCollider>(entity);
                launchData.colliderRemoved = true;
            }

            if (launchData.colliderRemoved && !launchData.colliderReattached)
            {
                launchData.launchTime -= deltaTime;
                if (launchData.launchTime <= -1f)
                {
                    ecb.AddComponent<PhysicsCollider>(entity, launchData.physicsCollider);
                    launchData.colliderReattached = true;
                }
            }
        }).Run();

        ecb.Playback(EntityManager);
        ecb.Dispose();

        return default;
    }
}
