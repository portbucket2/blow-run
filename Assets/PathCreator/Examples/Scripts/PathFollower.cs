﻿using UnityEngine;

namespace PathCreation.Examples
{
    // Moves along a path at constant speed.
    // Depending on the end of path instruction, will either loop, reverse, or stop at the end of the path.
    public class PathFollower : MonoBehaviour
    {
        public PathCreator pathCreator;
        public EndOfPathInstruction endOfPathInstruction;
        public float speed = 5;
        public float yOffset;
        public float sideOffset { get; set; }
        public float maxSideOffset;
        float distanceTravelled;
        bool pathCompleted;

        void Start()
        {
            if (pathCreator != null)
            {
                // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
                pathCreator.pathUpdated += OnPathChanged;
            }
        }

        void Update()
        {
            if (pathCreator != null)
            {
                distanceTravelled += speed * Time.deltaTime;
                sideOffset = Mathf.Clamp(sideOffset, -maxSideOffset, maxSideOffset);
                transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction) + sideOffset * transform.right + Vector3.up * yOffset;
                transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);

                if (!pathCompleted)
                {
                    CheckForPathComplete(distanceTravelled, LevelManager.Instance.StartFillingBallPit);
                }
            }
        }

        // If the path changes during the game, update the distance travelled so that the follower's position on the new path
        // is as close as possible to its position on the old path
        void OnPathChanged()
        {
            distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
        }

        void CheckForPathComplete(float distanceTravelled, System.Action OnPathCompleted = null)
        {
            if (distanceTravelled >= pathCreator.path.length)
            {
                Debug.Log(gameObject.name + " : " + "Path Complete");
                pathCompleted = true;
                OnPathCompleted?.Invoke();
            }
        }
    }
}